# run-iocsh conda recipe

Home: https://gitlab.esss.lu.se/ics-infrastructure/run-iocsh

Package license: BSD 2-Clause

Recipe license: BSD 3-Clause

Summary: Wrapper to test iocsh.bash
